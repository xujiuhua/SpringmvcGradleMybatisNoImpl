## 技术

SpringMVC + MySQL + MyBaits + Gradle

## 功能

登陆、记录登陆日志

## 扩展

1. Gradle 安装及配置
2. Spring 单元测试
3. 事务控制

## 集成MyBatis步骤

> 每一个 Mybatis 应用程序都以一个 SqlSessionFactory 对象的实例为核心

### 导入依赖包

```xml
'org.mybatis:mybatis:3.4.1',
'org.mybatis:mybatis-spring:1.3.0'
```

### applicationContext.xml 配置

```xml
<bean id="sqlSessionFactory"
  class="org.mybatis.spring.SqlSessionFactoryBean"
  p:dataSource-ref="dataSource"
  p:configLocation="classpath:myBatisConfig.xml"
  p:mapperLocations="classpath:mapper/*.xml"/>

<bean class="org.mybatis.spring.SqlSessionTemplate">
	<constructor-arg ref="sqlSessionFactory"/>
</bean>
```

### 使用映射接口

接口名称和映射命名空间相同，接口方法映射元素的id相同
如下配置即可将`接口`转换为Bean，可直接使用@Autowired注入，省略dao实现类

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer"
	p:basePackage="com.*.dao"/>
```
