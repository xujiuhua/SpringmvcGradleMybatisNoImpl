package com.baobaotao.dao;

import com.baobaotao.domain.User;
import org.apache.ibatis.annotations.Param;

public interface UserDao {

	int getMatchCount(@Param("userName")String userName, @Param("password")String password);

	User findUserByUserName(final String userName);

	void updateLoginInfo(User user);
}
