
## 安装配置Gradle

1. 从[Gradle](https://grdle.org/gradle-download/)官网下载完整解压包，并解压到本地
2. 配置环境变量
 - `GRADLE_HOME`:`D:\GreenSoft\gradle\gradle-3.1`
 - Path中添加`;%GRADLE_HOME%\bin`
3. 检验是否配置成功,命令行输入`gradle -v`

## 创建web工程
使用ide工具为Intellij Idea

### `New`-->`Project`

<img src="img/new-project.png">

<img src="img/new-project2.png">

### 添加 web.xml

> 默认工程创建完成无web.xml

<img src="img/add-web.png">

<img src="img/add-web2.png">

<img src="img/add-web3.png">

### 部署启动tomcat

## 集成SpringMVC

### 配置`build.gradle`

```gradle
def springVersion = '4.3.2.RELEASE'

dependencies {
    testCompile group: 'junit', name: 'junit', version: '4.12'

    // Spring
    compile(
            'org.springframework:spring-aop:' + springVersion,
            'org.springframework:spring-beans:' + springVersion,
            'org.springframework:spring-context:' + springVersion,
            'org.springframework:spring-core:' + springVersion,
            'org.springframework:spring-expression:' + springVersion,
            'org.springframework:spring-jdbc:' + springVersion,
            'org.springframework:spring-orm:' + springVersion,
            'org.springframework:spring-tx:' + springVersion,
            'org.springframework:spring-web:' + springVersion,
            'org.springframework:spring-webmvc:' + springVersion,
            'org.springframework:spring-test:' + springVersion
    )

    // Log\DB\Taglib
    compile(
            'log4j:log4j:1.2.17',
            'commons-logging:commons-logging:1.2',
            'commons-dbcp:commons-dbcp:1.4',
            'mysql:mysql-connector-java:5.1.6',
            'taglibs:standard:1.1.2',
            'javax.servlet.jsp.jstl:jstl-api:1.2'
    )

    // Tomcat
    compile(
            'org.apache.tomcat:tomcat-servlet-api:7.0.34',
            'org.apache.tomcat:tomcat-jsp-api:7.0.34'
    )

    compile(
            'org.mybatis:mybatis:3.4.1',
            'org.mybatis:mybatis-spring:1.3.0'
    )

}
```

### 单元测试Service

```java
// 基于JUnit4的Spring测试框架
@RunWith(SpringJUnit4ClassRunner.class) 
// 启动Spring容器
@ContextConfiguration(locations={"/applicationContext.xml"}) 
```
<img src="img/junit-test.png"/>

测试成功

<img src="img/test-pass.png"/>






